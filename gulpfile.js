/**
 * Created by JuliaPhoenix
 */
'use strict';

var gulp = require('gulp'),
		watch = require('gulp-watch'), // наблюдение за файлами
		uglify = require('gulp-uglify'), // сжатие JS
		pupm = require('pump'), // допоолнение для uglify подробнее https://github.com/terinjokes/gulp-uglify/blob/master/docs/why-use-pump/README.md#why-use-pump
		cleanСss = require('gulp-clean-css'), // сжатие CSS
		sass = require('gulp-sass'), // компиляция SASS (можно использовать LESS)
		sourcemaps = require('gulp-sourcemaps'), // для генерации css sourscemaps, которые будут помогать нам при отладке кода https://www.npmjs.com/package/gulp-autoprefixer
		prefixer = require('gulp-autoprefixer'), // автоматически добавляет вендорные префиксы к CSS свойствам
		rigger = require('gulp-rigger'), // позволяет импортировать один файл в другой простой конструкцией (//= footer.html)
		imagemin = require('gulp-imagemin'), // для сжатия картинок
		// cssBase64 = require('gulp-css-base64'), // для преобразования картинок в формат base64^ преобразует все url('img') внутри css
	  // urlAdjuster = require('gulp-css-url-adjuster'), // смена адреса
		pug = require('gulp-pug'),
		rimraf = require('rimraf'), // rm -rf для ноды, нужна для задачи по очистке папки build
		browserSync = require("browser-sync"), // развернем локальный dev сервер с блэкджеком и livereload
		reload = browserSync.reload;

var path = {
			build: { //Тут мы укажем куда складывать готовые после сборки файлы
				js: 'build/js/',
				img: 'build/img/',
				html: 'build/html',
				style: 'build/css/',
				fonts: 'build/fonts/'
			},
			src: { //Пути откуда брать исходники
				js: 'src/js/main.js', //Только main файлы для js
				img: 'src/components/**/img/*.*', // Все img внутри компонентов
				html: 'src/html/*.pug', //Синтаксис src *.html говорит gulp что мы хотим взять все файлы с расширением .html
				style: 'src/scss/main.scss', //Только main файлы для css
				styleMobile: 'src/scss/mobile.scss', //Только main файлы для css
				fonts: 'src/fonts/**/*.*' // Берем все шрифты

			},
			watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
				js: 'src/components/**/*.js', // Следим за изменением в main.js и за всеми js внутри компонентов
				img: 'src/components/**/img/*.*',  // Следим за изменением всех img внутри компонентов
				html: 'src/components/**/*.pug', // Следим за изменением в корневых html и за всеми html внутри компонентов
				style: 'src/components/**/*.scss', // Следим за изменением в main.scss и за всеми scss внутри компонентов
				styleMobile: 'src/components/**/*.scss',
				fonts: 'src/fonts/**/*.*'
			},
			clean: './build'
};

var config = {
		server: {
			baseDir: "./build"
		},
		open: false,
		tunnel: false,
		host: 'localhost',
		port: 3303,
		logPrefix: "jp",
		notify: false
};

gulp.task('html:build', function(){
	gulp.src(path.src.html) //Выберем файлы по нужному пути
		// .pipe(rigger()) //Прогоним через rigger
		.pipe(pug()) // Преобразуем в html
		.pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
		.pipe(reload({stream: true})); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function () {
	gulp.src(path.src.js) //Найдем наш main файл
		.pipe(rigger()) //Прогоним через rigger
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
		.pipe(uglify()) //Сожмем наш js
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
		.pipe(reload({stream: true})); //И перезагрузим сервер
});


gulp.task('style:build', function () {
	gulp.src(path.src.style) //Выберем наш main.scss
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
		.pipe(sass()) //Скомпилируем scss в css
		// .pipe(cssBase64()) // Кодируем картинки
		.pipe(prefixer({browsers: ['last 2 versions']})) //Добавим вендорные префиксы
    // .pipe(urlAdjuster({
    //     prepend: '../'
    // }))
		.pipe(cleanСss()) // Сожмем css
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.style)) //И в build
		.pipe(reload({stream: true}));
});

gulp.task('styleMobile:build', function () {
	gulp.src(path.src.styleMobile) //Выберем наш mobile.scss
		.pipe(sourcemaps.init()) //Инициализируем sourcemap
		.pipe(sass()) //Скомпилируем scss в css
		// .pipe(cssBase64()) // Кодируем картинки
		.pipe(prefixer({browsers: ['last 2 versions']})) //Добавим вендорные префиксы
    // .pipe(urlAdjuster({
    //     prepend: '../'
    // }))
		.pipe(cleanСss()) // Сожмем css
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.style)) //И в build
		.pipe(reload({stream: true}));
});

// копирование и сжатие картинок
gulp.task('image:build', function () {
    gulp.src(path.src.img) //Выберем наши картинки
        .pipe(imagemin())
        .pipe(gulp.dest(path.build.img)) //И бросим в build
        .pipe(reload({stream: true}));
});

// копирование шрифтов
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});


gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'styleMobile:build',
	'fonts:build',
	'image:build'
]);

gulp.task('watch', function(){
	watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function(event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.styleMobile], function(event, cb) {
		gulp.start('styleMobile:build');
	});
	watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	watch([path.watch.img], function(event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
});

// запуск сервера на прослушку
gulp.task('webserver', function () {
	browserSync(config);
});

// Очистка папки build
gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

// Запуск всей сборки
gulp.task('dev', ['build', 'webserver', 'watch']);
