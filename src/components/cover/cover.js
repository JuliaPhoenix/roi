$( document ).ready(function() {

  if (localStorage) {

    if (localStorage.name) {
      $('[data-type=name]').html(localStorage.name);
    }
    if (localStorage.country) {
      $('[data-type=country]').html(localStorage.country);
    }
    if (localStorage.languages) {
      $('[data-type=languages]').html(localStorage.languages);
    }
    if (localStorage.skillsAdd) {
      $('.cover__info__skills-add input').val(JSON.parse(localStorage.skillsAdd));
      for (var i = 0; i < JSON.parse(localStorage.skillsAdd).length; i++) {
        $('<div class="cover__info__skills__item"><div class="cover__info__skills__item__tag">'+JSON.parse(localStorage.skillsAdd)[i]+'</div><div class="cover__info__skills__item__delete js-delete-tag">x</div></div>').appendTo('.cover__info__skills');
      }
    }

  }

  // клик по элементу с классом .js-edit
  $('.js-edit').click(function() {
    var el = $(this), //элемент по которому был клик (текущий)
        dataType = el.attr('data-type'), //получаем значение атрибута текущего элемента
        parent = el.parent(), // определяем родителя текущего элемента (блок)
        form = parent.find('form'), //находим форму в блоке
        input = form.find('input'), //находим input в блоке
        submit = form.find('[type="submit"]'); //находим кнопку отправить в блоке

    // если атрибут текущего элемента == skillsAdd, то ищем select
    if (dataType=='skillsAdd') {
      var select = form.find('select'); // находим select в текущей форме
    }

    el.addClass('hide'); // скрываем текстовый блок
    form.addClass('active'); // показываем форму

    /* если значение атрибута текущего элемента не равно skillsAdd,
    то выполняем код ниже */
    if (!dataType=='skillsAdd') {
      // при изменение inputa выполняем код ниже
      $(input).change(function() {
        var valInput = input.val(); // присваивае значение инпута переменной

        el.html(valInput); // Меняем содержимое в тектовом блоке на значение инпута
        localStorage[dataType] = valInput; // записываем изменение в хранилище
      });
    } else { //если значение атрибута текущего элемента равно skillsAdd, то выполняем код ниже
      //если в хранилище есть данные по ключу skillsAdd
      if (localStorage.skillsAdd) {
        var tags= JSON.parse(localStorage.skillsAdd); // приваиваем значения из хранилица переменной
      } else {
        var tags= []; // присваиваем переменной пустой массив
      }

      // при изменнеии селекта, выполняем код ниже
      $(select).change(function() {
        var selectVal = select.val(), // получаем значение селекта
            tagsBox = $('.cover__info__skills'); //находи блок вывода тегов

        tags.push(selectVal); // пушим выбранное значение в массив тегов
        localStorage[dataType] = JSON.stringify(tags);// записываем выбранное значение в хранилище
        $('<div class="cover__info__skills__item"><div class="cover__info__skills__item__tag">'+selectVal+'</div><div class="cover__info__skills__item__delete js-delete-tag">x</div></div>').appendTo(tagsBox);
        input.val(tags); //выводим значения в инпуте
      });
    }


    // при клике на кнопку с типом submit, выполняем код ниже
    $(submit).click(function() {
      el.removeClass('hide'); // удаляем класс у текущего элемента
      form.removeClass('active'); // удаляем класс у формы
    });


  })
  var tagsExist = [];
  $('.js-delete-tag').click(function deleteItem() {
    console.log('after');
    var el = $(this),
        parent = el.parent();
        tag = el.prev().html();
        tagsBox = $('.cover__info__skills'); //находи блок вывода тегов


    for (var i = 0; i < JSON.parse(localStorage.skillsAdd).length; i++) {
      if (JSON.parse(localStorage.skillsAdd)[i] !== tag) {
        tagsExist.push(JSON.parse(localStorage.skillsAdd)[i]);
      } else {
        parent.detach();
      }
    }

    localStorage.skillsAdd = JSON.stringify(tagsExist);

  });

});
